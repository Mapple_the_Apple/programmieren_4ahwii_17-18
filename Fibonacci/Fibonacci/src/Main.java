
public class Main {
	
	private static int fac(int n, int m){
		
		if (n==0) return m;
		return fac(n-1,n*m);
	}
	
	public static int facRecEnd(int n){
		return fac(n,1);
	}
	
	public static void main(String[] args) {
		System.out.println(facRecEnd(9));

	}

}
