import javax.swing.plaf.FontUIResource;

public class MyLinkedList {
	private Node head = null;
	private Node lastNode = null;
	private int size = 0;
	
	public int getSize() {
		return size;
	}
	
	public int getValue(int index) {
		if ( index < 0 || index > this.size ) {
			throw new IndexOutOfBoundsException();
		}
		Node n = this.head;
		for (int i = 0; i < index; i++) {
			n = n.getNext();
		}
		return n.getValue();
	}

	public void add(int value) {
		Node n = new Node();
		n.setValue(value);
		
		if ( this.head == null ) {
			this.head = n;
		} else {
			this.lastNode.setNext(n);
		}
		this.lastNode = n;
		this.size++;
	}
	
	public void delete( int index) {
		if ( index < 0 ) {
			System.err.println("Specify index >= 0");
			return;
		}
		
		if ( this.head == null ) {
			System.err.println("No elements in list");
			return;
		}
		
		if ( index == 0 ) {
			if ( this.head.hasNext()) {
				this.head = this.head.getNext();
				this.size--;
				return;
			} else {
				this.head = null;
				this.size--;
				return;
			}
		}
		
		// Node in list 
		Node n = this.head; 
		for (int i = 0; i < index-1; i++) {
			if (n.hasNext()) {
				n = n.getNext();
			} else {
				System.err.println("Index out of bounds");
				throw new IndexOutOfBoundsException();
			}
		}
		
		if ( n.getNext().hasNext() ) {
			n.setNext(n.getNext().getNext());
		} else {
			lastNode = n;
			n.setNext(null);
		}
		this.size--;
		
	}
	
	public Node getHead() {
		return this.head;
	}
}
