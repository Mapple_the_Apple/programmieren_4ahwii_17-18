
public class Node {
	private int value;
	private Node next =null;
	
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	public boolean hasNext() {
		if ( this.next != null ) {
			return true;
		}else {
			return false;
		}
	}

	public void setNext( Node next ) {
		this.next = next;
	}
	
	public Node getNext() {
		return this.next;
	}
	
}
