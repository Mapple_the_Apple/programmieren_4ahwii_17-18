
public class Main {
	
	public static String rekursivität(String palindrom){
		

		palindrom.toLowerCase();
		
		if(palindrom.length()==1 || palindrom.length()==0){
		return("ist ein Palindrom.");
		}
		
		if(palindrom.charAt(0)==palindrom.charAt(palindrom.length()-1)){
			
			palindrom = palindrom.substring(0,1);
			palindrom = palindrom.substring(palindrom.length()-1, palindrom.length());
			return rekursivität(palindrom);
		}
		else{
			return("ist kein Palindrom.");
		}
	}

	public static void main(String[] args) {
		System.out.println(rekursivität("Regallager"));

	}

}
