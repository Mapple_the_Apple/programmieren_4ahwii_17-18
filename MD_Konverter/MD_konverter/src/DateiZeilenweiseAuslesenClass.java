import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File; 
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays; 

public class DateiZeilenweiseAuslesenClass { 
	static ArrayList<String> text = new ArrayList<String>();

	private static void ladeDatei(String datName) { 

		File file = new File(datName); 

		if (!file.canRead() || !file.isFile()) 
			System.exit(0); 

		BufferedReader in = null; 
		try { 
			in = new BufferedReader(new FileReader(datName)); 
			String zeile = null; 
			while ((zeile = in.readLine()) != null) { 
				System.out.println(zeile); 
				text.add(zeile); 

			} 
		} catch (IOException e) { 
			e.printStackTrace(); 
		} finally { 
			if (in != null) 
				try { 
					in.close(); 
				} catch (IOException e) { 
				} 
		} 
	} 

	public static void replace(){

		int x=0;
		for(int i =0; i < text.size();i++){

			if(text.get(i).contains("###### ")){
				String text0=text.get(i).replaceAll("###### ", "<h6>");
				System.out.println(text0 + "</h6>");
			}
			else if(text.get(i).contains("##### ")){
				String text0=text.get(i).replaceAll("##### ", "<h5>");
				System.out.println(text0 + "</h5>");
			}
			else if(text.get(i).contains("#### ")){
				String text0=text.get(i).replaceAll("#### ", "<h4>");
				System.out.println(text0 + "</h4>");
			}
			else if(text.get(i).contains("### ")){
				String text0=text.get(i).replaceAll("### ", "<h3>");
				System.out.println(text0 + "</h3>");
			}
			else if(text.get(i).contains("## ")){
				String text0=text.get(i).replaceAll("## ", "<h2>");
				System.out.println(text0 + "</h2>");
			}
			else if(text.get(i).contains("# ")){
				String text0=text.get(i).replaceAll("# ", "<h1>");
				System.out.println(text0 + "</h1>");
			}
			if(text.get(i).contains("* ")){
				x++;
				if(x==1)System.out.print("<ul>");
				String text0=text.get(i).replace("* ", "<li>");
				System.out.print(text0 + "</li>");
				if(text.get(i+1).contains("* ")==false){
					System.out.print("</ul>");
					x=0;
				}
			}
			else{
				System.out.println(text.get(i));
			}

			while((text.get(i).contains("_"))||(text.get(i).contains("*"))){
				
				System.out.println(text.get(i));
				
				if(text.get(i).contains("___")){
					text.set(i, text.get(i).replaceFirst("___", "<i><b>"));
					text.set(i, text.get(i).replaceFirst("___", "</i></b>"));
					
				}
				else if(text.get(i).contains("__")){
					text.set(i, text.get(i).replaceFirst("__", "<b>"));
					text.set(i, text.get(i).replaceFirst("__", "</b>"));
				}
				else if(text.get(i).contains("_")){
					text.set(i, text.get(i).replaceFirst("_", "<i>"));
					text.set(i, text.get(i).replaceFirst("_", "</i>"));
				}
				else if(text.get(i).contains("***")){
					text.set(i, text.get(i).replaceFirst("[*][*][*]", "<i><b>"));
					text.set(i, text.get(i).replaceFirst("[*][*][*]", "</i></b>"));
				}
				else if(text.get(i).contains("**")){
					text.set(i, text.get(i).replaceFirst("[*][*]", "<b>"));
					text.set(i, text.get(i).replaceFirst("[*][*]", "</b>"));
				}
				else if(text.get(i).contains("*")){
					text.set(i, text.get(i).replaceFirst("[*]", "<i>"));
					text.set(i, text.get(i).replaceFirst("[*]", "</i>"));
				}
				
				
				
			}
			text.set(i,text.get(i)+"<br/>");
			//System.out.println(text.get(i));
			System.out.println();
		}

		


	}


	public static void main(String[] args) throws IOException { 
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Bitte Dateipfad angeben:");

		String dateiName = console.readLine(); 
		ladeDatei(dateiName); 
		System.out.println("_____________________________________");

		replace();
		

		String html="";
        for(int i=0; i<=text.size()-1;i++){
            html+=text.get(i);
        }
        try{
            dateiName = dateiName.replaceFirst(".md", "") + ".html";
            BufferedWriter bw=new BufferedWriter(new FileWriter(dateiName));
            bw.write(html);
            bw.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

	} 
}