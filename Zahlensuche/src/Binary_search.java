import java.util.ArrayList;

public class Binary_search {
	
	static ArrayList<Integer> values = new ArrayList<Integer>();
	
	
	
	
	public static int binarySearch(Integer value, ArrayList<Integer> al){
		
		return suche(al, value,0,al.size());
	}
	
	private static int suche(ArrayList <Integer> werte, int value, int start, int ende){
		int c = (start+ende)/2;
		
		if(werte.get(c)==value)
		{
			return(c);
		}
		if(werte.get(c) < value){
			return (suche(werte,value, c, start));	
		}
		if(werte.get(c)>value){
			return (suche(werte,value,c, ende));
		}
		if(start==ende){
			return(-1);
		}
		
		
		
	}
	

	public static void main(String[] args) {
		for (int i=0;i<=10;i++){
			values.add(i);
		}
		System.out.println(binarySearch(7,values));
		
	}

}
